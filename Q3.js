function isPalindrome(){

	var string = document.getElementById("palin").value;
	var retVal=checkpalin(string.toLowerCase());
	if(retVal==true){
		document.write("<h1> Given String is Palindrome</h1>");
	}
	else
	{
		document.write("<h1> Given String is not Palindrome</h1>");
	}
	
}

function checkpalin(str){
	if(str.length==0 || str.length==1)
		return true;
	if(str.charAt(0)==str.charAt(str.length-1)){
		return checkpalin(str.substring(1,str.length-1));
	}
		return false;

}