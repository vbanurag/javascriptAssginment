(function() {
  'use strict';

var emp_detail1 = {name:"anuj",age:25,sal:1550,dob:new Date(1992,0,21)};
var emp_detail2 = {name:"anurag",age:25,sal:12500,dob:new Date(1992,1,21)};
var emp_detail3 = {name:"rahul",age:25,sal:30000,dob:new Date(1994,5,20)};
var emp_detail4 = {name:"nitin",age:26,sal:800,dob:new Date(1996,6,11)};
var emp_detail5 = {name:"aniket",age:20,sal:1500,dob:new Date(1990,1,21)};
var emp_detail6 = {name:"shaswat",age:25,sal:900,dob:new Date(1994,8,21)};
var emp_detail7 = {name:"vivek",age:22,sal:1515,dob:new Date(1992,6,20)};

var allemp =[emp_detail1,emp_detail2,emp_detail3,emp_detail4,emp_detail5,emp_detail6,emp_detail7];

//ilter all employees with salary greater than 5000
console.log("Filter all emp sal grater than 5000: \n");
for(var i =0;i<allemp.length;i++){
    if(allemp[i].sal>5000){
        console.log(allemp[i].name+" "+allemp[i].age+" "+allemp[i].sal+" "+allemp[i].dob);}}

//fetch employees with salary less than 1000 and age greater than 20. Then give them an increment 5 times their salary.
console.log("\nEmployee whose sal less than 1000 and age greater than 20: \n")
for(var i =0;i<allemp.length;i++){
    if(allemp[i].sal<1000 && allemp[i].age>20){
        allemp[i].sal *=5;}}
for(var i =0; i<allemp.length;i++){
	console.log(allemp[i].name+" "+allemp[i].age+" "+allemp[i].sal+" "+allemp[i].dob);};

//group employee on the basis of their age
console.log("\n\nGroup Employee on the basis of their age: \n");
allemp.sort(function(x,y){
	return x.age-y.age;
});
for(var i =0; i<allemp.length;i++){
	console.log(allemp[i].name+" "+allemp[i].age+" "+allemp[i].sal+" "+allemp[i].dob);};

	}());